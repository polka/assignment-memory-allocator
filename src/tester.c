#include <stdio.h>
#include <unistd.h>
#include "mem.h"
#include "mem_internals.h"
#include "util.h"

#define SIZE 4096
#define MAP_ANONYMOUS 0x20
#define MAP_FIXED_NOREPLACE 0x100000

static struct block_header *bh;
static struct block_header *all_bh;
static void *pointer_heap;

void test_1(){
  printf("\n--------TEST 1--------\n");
  pointer_heap = heap_init(SIZE);
  if(pointer_heap != NULL){
     printf("\n--------!!!!--------\n");
    //start block
    bh = (struct block_header*) (pointer_heap);
    printf("Test_1: heap is initialized\n");
    debug_heap(stdout, pointer_heap);
    printf("Test_1: INFO: finished successfully\n");
    return;
  }
  printf("Test_1: ERROR: failed");
}

void test_2(){
  printf("\n--------TEST 2--------\n");
  void *mem = _malloc(512);
  if (mem == NULL) printf("Test_2: ERROR: failed -> memory allocation error, nullpointer\n");
  if (bh -> capacity.bytes != 512) printf("Test_2: ERROR: failed -> size doesnt match\n");
  if (bh -> is_free) printf("Test_2: ERROR: failed -> block marked as free\n");
  
  debug_heap(stdout, pointer_heap);
  all_bh = mem;
  
  printf("Test_2: INFO: finished successfully\n");
  return;
}

void test_3(){
  printf("\n--------TEST 3--------\n");
  _free(all_bh);
  
  if(!bh -> is_free) printf("Test_3: ERROR: failed -> block marked as not free\n");
  
  debug_heap(stdout, pointer_heap);
  printf("Test_3: INFO: finished successfully\n");
  return;
}

void test_4(){
  printf("\n--------TEST 4--------\n");
  void *point_1 = _malloc(512);
  void *point_2 = _malloc(512);
  debug_heap(stdout, pointer_heap);
  
  _free(point_1);
  _free(point_2);
  
  if(!bh -> is_free) printf("Test_4: ERROR: failed -> block marked as not free\n");
  
  debug_heap(stdout, pointer_heap);
  printf("Test_4: INFO: finished successfully\n");
  return;
}

void test_5(){
  printf("\n--------TEST 5--------\n");
  struct block_header *bh_2;
  void *mem = _malloc(2*SIZE);
  debug_heap(stdout, pointer_heap);
  
  if (mem == NULL) printf("Test_5: ERROR: failed -> memory allocation error, nullpointer\n");
  
  bh_2 = (struct block_header*) ((uint8_t*) mem - offsetof(struct block_header, contents));
  if(bh_2 -> capacity.bytes != 2*SIZE) printf("Test_5: ERROR: failed -> size doesnt match\n");
  if(bh_2 -> is_free) printf("Test_5: ERROR: failed -> memory allocation error, block marked as free\n");
  
  _free(mem);
  if(!bh_2 -> is_free) printf("Test_5: ERROR: failed -> memory allocation error, block marked as not free\n");
  
  debug_heap(stdout, pointer_heap);
  printf("Test_5: INFO: finished successfully\n");
  return;
}

void test_6(){//rasshirenie kuchi s videleniem regiona v drugom meste
    printf("\n--------TEST 6--------\n");
    struct block_header *current = bh;
    void *pointer;
    
    void *d1 = _malloc(20500);
    if (d1 == NULL) printf("Test_6: ERROR: failed -> memory allocation error, nullpointer\n");
    
    while(current -> next != NULL){
      current = current -> next;
    }
    pointer = mmap((uint8_t*) current -> contents + current -> capacity.bytes, 20000, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, 0, 0);
    
    if (pointer == NULL || pointer == MAP_FAILED) printf("Test_5: ERROR: failed -> memory allocation error, nullpointer\n");
    debug_heap(stdout, pointer_heap);
    
    void *d2 = _malloc(90500);
    struct block_header *bh_2 = (struct block_header*) ((uint8_t*) d2 - offsetof(struct block_header, contents));
    if ((uint8_t*) bh_2 == (uint8_t*) current -> contents + current -> capacity.bytes) printf("Test_5: ERROR: failed -> block one after another\n");
    
    _free(d1);
    _free(d2);
     printf("Test_6: INFO: finished successfully\n");
}

int main(){
  printf("\n--------TESTER START--------\n");
  test_1();
  test_2();
  test_3();
  test_4();
  test_5();
  test_6();
  printf("\n--------TESTER END--------\n");
  return 0;
}

